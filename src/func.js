const getSum = (str1, str2) => {
  if (typeof str1 === "string" && typeof str2 === "string" && !isNaN(Number(str1 + str2))) {
    return (+str1 + +str2).toString();
  }
  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const postsCount = listOfPosts.filter((post) => post.author === authorName).length;

  const commentssCount = listOfPosts
    .filter((post) => "comments" in post)
    .map((post) => post.comments)
    .flat()
    .filter((comment) => comment.author === authorName).length;

  return `Post:${postsCount},comments:${commentssCount}`;
};

const tickets = (people) => {
  const ticketCost = 25;
  let isSucceeded = "YES";
  const bills = {
    25: 0,
    50: 0,
    100: 0,
  };

  people.forEach((bill, index) => {
    bills[bill] = bill in bills ? bills[bill] + 1 : 1;

    let change = bill - ticketCost;

    if (change === 0) {
      return;
    }

    const billsTypes = Object.keys(bills)
      .filter((key) => bills[key] > 0 && key !== bill)
      .reverse();

    billsTypes.forEach((billType) => {
      if (change < billType) {
        return;
      } else {
        const billTypeNeeded = Math.floor(change / billType);

        for (let i = billTypeNeeded; i > 0; i--) {
          if (bills[billType] >= i) {
            change = change - billType * i;
            bills[billType] -= i;
          } else {
            isSucceeded = "NO";
            return;
          }
        }
      }
    });

    if (change > 0) {
      isSucceeded = "NO";
      people.splice(index);
    }
  });

  return isSucceeded;
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
